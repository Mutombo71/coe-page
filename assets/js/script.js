document.addEventListener("DOMContentLoaded", function () {

  // Get the main content element
  const mainContent = document.getElementById("main-content");

  // Function to load content from HTML files
  function loadContent(contentName) {
    const file = `content/${contentName}.html`;
    fetch(file)
      .then((response) => response.text())
      .then((data) => {
        mainContent.innerHTML = data;

        if (contentName === "redhat") {
          loadDynamicScript("assets/js/redhat.js");
        }

      })
      .catch((error) => {
        mainContent.innerHTML = "<p>Error loading content.</p>";
        console.error("Error loading content:", error);
      });
  }

  function getPageIdentifierFromURL() {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get("page");
  }

  // Handle navigation item clicks
  const navLinks = document.querySelectorAll("nav a");
  navLinks.forEach((link) => {
    link.addEventListener("click", function (event) {
      event.preventDefault();
      const contentName = this.getAttribute("data-content");
      loadContent(contentName);
    });
  });

  // Load the main content on page load (optional)
  const pageIdentifier = getPageIdentifierFromURL();
  if (pageIdentifier) {
    loadContent(pageIdentifier);
  } else {
    loadContent("main");
  }

  // Load the header content
  /* const headerContent = document.getElementById("header-content");
  fetch("includes/header.html")
    .then((response) => response.text())
    .then((data) => {
      headerContent.innerHTML = data;
    })
    .catch((error) => {
      headerContent.innerHTML = "<p>Error loading header.</p>";
      console.error("Error loading header:", error);
    }); */

  // Load the footer content
  const footerContent = document.getElementById("footer-content");
  fetch("includes/footer.html")
    .then((response) => response.text())
    .then((data) => {
      footerContent.innerHTML = data;
    })
    .catch((error) => {
      footerContent.innerHTML = "<p>Error loading footer.</p>";
      console.error("Error loading footer:", error);
    });

  // Function to load a dynamic JavaScript file
  function loadDynamicScript(src) {
    const script = document.createElement("script");
    script.src = src;
    script.async = true;
    document.getElementById("dynamic-script-container").appendChild(script);
  }
});

document.getElementById('surveyForm').onsubmit = function (event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    const mailtoUrl = 'mailto:dredak@techdata.com'

    const subject = encodeURIComponent('Technical Demonstration Workshop Survey');
    let body = '';
    formData.forEach((value, key) => {
        body += `${encodeURIComponent(key)}=${encodeURIComponent(value)}\n`;
    });

    // Construct the mailto link with pre-filled subject and body
    const mailtoLink = `${mailtoUrl}?subject=${subject}&body=${body}`;

    // Open the user's default email client with the pre-filled data
    window.location.href = mailtoLink;
};

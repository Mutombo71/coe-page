document.getElementById('surveyForm').onsubmit = function (event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    const mailtoUrl = 'mailto:dredak@techdata.com'

    const subject = encodeURIComponent('Technical Demonstration Workshop Survey');
    let body = '';
    formData.forEach((value, key) => {
        body += `${encodeURIComponent(key)}=${encodeURIComponent(value)}\n`;
    });

    // Construct the mailto link with pre-filled subject and body
    const mailtoLink = `${mailtoUrl}?subject=${subject}&body=${body}`;

    // Open the user's default email client with the pre-filled data
    window.location.href = mailtoLink;
};

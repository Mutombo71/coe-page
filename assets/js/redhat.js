let jsonData = null;
        let startIndex = 0;
        const batchSize = 5;
        const jsonRhFilePath = 'content/demo_catalog.json';

        async function loadJSON() {
            try {
                const response = await fetch(jsonRhFilePath);
                jsonData = await response.json();
                showBatch();
            } catch (error) {
                console.error('Error loading JSON:', error);
            }
        }

        function showBatch() {
            const dataTable = document.getElementById('data-table');
            dataTable.innerHTML = ''; // Clear previous content

            // Create header row
            const headerRow = document.createElement('div');
            headerRow.classList.add('table-header');
            const headerTopic = document.createElement('div');
            headerTopic.classList.add('table-cell');
            headerTopic.textContent = 'Topic';
            const headerDescription = document.createElement('div');
            headerDescription.classList.add('table-cell');
            headerDescription.textContent = 'Description';
            const headerDuration = document.createElement('div');
            headerDuration.classList.add('table-cell');
            headerDuration.textContent = 'Duration';
            headerRow.appendChild(headerTopic);
            headerRow.appendChild(headerDescription);
            headerRow.appendChild(headerDuration);
            dataTable.appendChild(headerRow);

            for (let i = startIndex; i < Math.min(startIndex + batchSize, jsonData.length); i++) {
                const tableRow = document.createElement('div');
                tableRow.classList.add('table-row');

                const topicCell = document.createElement('div');
                topicCell.classList.add('table-cell');
                topicCell.textContent = jsonData[i].topic;

                const descriptionCell = document.createElement('div');
                descriptionCell.classList.add('table-cell');
                descriptionCell.textContent = jsonData[i].description;

                const durationCell = document.createElement('div');
                durationCell.classList.add('table-cell');
                durationCell.textContent = jsonData[i].duration;

                tableRow.appendChild(topicCell);
                tableRow.appendChild(descriptionCell);
                tableRow.appendChild(durationCell);

                dataTable.appendChild(tableRow);
            }

            if (startIndex + batchSize >= jsonData.length) {
                document.getElementById('loadMore').style.display = 'none';
            } else {
                document.getElementById('loadMore').style.display = 'block';
            }

            if (startIndex === 0) {
                document.getElementById('backToBeginning').style.display = 'none';
            } else {
                document.getElementById('backToBeginning').style.display = 'block';
            }
        }

        document.getElementById('loadMore').addEventListener('click', () => {
            startIndex += batchSize;
            showBatch();
        });

        document.getElementById('backToBeginning').addEventListener('click', () => {
            startIndex = 0;
            showBatch();
        });

        loadJSON();
